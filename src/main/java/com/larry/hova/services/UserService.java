package com.larry.hova.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.larry.hova.models.CatPhoneType;
import com.larry.hova.models.User;
import com.larry.hova.models.UserPhone;
import com.larry.hova.models.UsersResponse;
import com.larry.hova.repository.CatPhoneTypeRepo;
import com.larry.hova.repository.UserPhoneRepo;
import com.larry.hova.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserService {

    private final int MASCULINO = 1;
    private final int FEMENINO = 2;

    @Autowired
    CatPhoneTypeRepo catPhoneTypeRepo;

    @Autowired
    UserPhoneRepo userPhoneRepo;

    @Autowired
    UserRepo userRepo;

    @Autowired
    ObjectMapper objectMapper;

    public UsersResponse getUsers(ObjectNode body) {
        UsersResponse response = new UsersResponse();
        List<User> users = null;
        try {
            if (!body.has("searchby")) {
                throw new Exception("Debe ingresar un valor para la llave searchby");
            }
            String query = body.get("searchby").asText();
            String values[] = query.split(",");
            if (values.length == 2) {
                users = userRepo.findAllByNameContainingAndRfcContaining(values[0], values[1]);
            } else {
                users = userRepo.findAllByNameContaining(values[0]);
                if (users.size() == 0) {
                    users = userRepo.findAllByRfcContaining(values[0]);
                }
            }

            response.setItems(users);
            response.setResults(users.size());
        } catch (Exception e) {
            response.setResults(0);
        }
        return response;
    }

    public List<CatPhoneType> getCatPhoneType() {
        return catPhoneTypeRepo.findAllByIdPhoneType(1);
    }

    @Transactional
    public ObjectNode saveOrUpdate(User user) {
        ObjectNode objectNode = objectMapper.createObjectNode();
        int id = 0;

        try {
            if (user.getName() == null || user.getName().trim().isEmpty()) {
                throw new Exception("Es necesario el nombre");
            }
            if (user.getLastname() == null || user.getLastname().trim().isEmpty()) {
                throw new Exception("Es necesario el apellido paterno");
            }
            if (user.getSurname() == null || user.getSurname().trim().isEmpty()) {
                throw new Exception("Es necesario el apellido materno");
            }
            if (user.getRfc() == null || user.getRfc().trim().isEmpty()) {
                throw new Exception("Es necesario el rfc");
            }
            if (user.getWorkshift() == null || user.getWorkshift() <= 0) {
                throw new Exception("Es necesario el turno");
            }
            if (user.getGender() != MASCULINO && user.getGender() != FEMENINO) {
                throw new Exception("Es necesario un género válido: 1 / 2");
            }
            if (user.getPin() == null || user.getPin() <= 0) {
                throw new Exception("Es necesario un PIN");
            } else {
                if (!validarPIN(user.getPin())) {
                    objectNode.put("pin", false);
                    throw new Exception("El PIN ya existe, probar con otro.");
                } else {
                    objectNode.put("pin", true);
                }
            }

            if (user.getBirthdate() == null) {
                throw new Exception("Es necesaria la fecha de nacimiento");
            }

            id = userRepo.save(user).getKeyUser();

            if (!user.getUserPhonesByKeyUser().isEmpty()) {
                for (UserPhone phone : user.getUserPhonesByKeyUser()) {
                    phone.setKeyUser(id);
                    userPhoneRepo.save(phone);
                }
            }

            if (id != 0) {
                objectNode.put("id", id);
            } else {
                throw new Exception("Error en la inserción o actualización");
            }

        } catch (Exception e) {
            objectNode.put("error", e.getMessage());

        }
        return objectNode;
    }

    public boolean validarPIN(Long pin) {
        return userRepo.countAllByPin(pin) == 0;
    }
}
