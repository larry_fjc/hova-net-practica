package com.larry.hova.repository;

import com.larry.hova.models.UserPhone;
import org.springframework.data.repository.CrudRepository;

public interface UserPhoneRepo extends CrudRepository<UserPhone, Integer> {
}
