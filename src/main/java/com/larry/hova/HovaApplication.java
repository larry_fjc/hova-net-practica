package com.larry.hova;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class HovaApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(HovaApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(HovaApplication.class);
	}

	@RestController
	class Hello {
		@GetMapping("/hello/{name}")
		String hello(@PathVariable String name) {
			return "Hi " + name + "!";
		}
	}

}
