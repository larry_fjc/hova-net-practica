package com.larry.hova.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cat_profile", schema = "hova_net", catalog = "")
public class CatProfile {
    private Integer idProfile;
    private String description;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_profile")
    public Integer getIdProfile() {
        return idProfile;
    }

    public void setIdProfile(Integer idProfile) {
        this.idProfile = idProfile;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CatProfile that = (CatProfile) o;
        return Objects.equals(idProfile, that.idProfile) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idProfile, description);
    }
}
