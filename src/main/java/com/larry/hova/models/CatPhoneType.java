package com.larry.hova.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cat_phone_type", schema = "hova_net", catalog = "")
public class CatPhoneType {
    private Integer idPhoneType;
    private String description;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_phone_type")
    public Integer getIdPhoneType() {
        return idPhoneType;
    }

    public void setIdPhoneType(Integer idPhoneType) {
        this.idPhoneType = idPhoneType;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CatPhoneType that = (CatPhoneType) o;
        return Objects.equals(idPhoneType, that.idPhoneType) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPhoneType, description);
    }
}
