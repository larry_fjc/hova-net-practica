package com.larry.hova.repository;

import com.larry.hova.models.CatPhoneType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CatPhoneTypeRepo extends CrudRepository<CatPhoneType, Integer> {
    List<CatPhoneType> findAllByIdPhoneType(Integer idPhoneType);
}
