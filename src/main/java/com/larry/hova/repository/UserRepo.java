package com.larry.hova.repository;

import com.larry.hova.models.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepo extends CrudRepository<User, Integer> {
    List<User> findAllByNameContainingAndRfcContaining(String name, String rfc);
    List<User> findAllByNameContaining(String name);
    List<User> findAllByRfcContaining(String rfc);
    int countAllByPin(Long pin);
}
