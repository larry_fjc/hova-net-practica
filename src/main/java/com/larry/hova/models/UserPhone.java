package com.larry.hova.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_phone", schema = "hova_net", catalog = "")
public class UserPhone {
    private Integer idPhone;
    private String phone;
    private User userByKeyUser;
    private CatPhoneType catPhoneTypeByIdPhoneType;
    private Integer keyUser;
    private Integer idPhoneType;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_phone")
    public Integer getIdPhone() {
        return idPhone;
    }

    public void setIdPhone(Integer idPhone) {
        this.idPhone = idPhone;
    }

    @Basic
    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPhone userPhone = (UserPhone) o;
        return Objects.equals(idPhone, userPhone.idPhone) &&
                Objects.equals(phone, userPhone.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPhone, phone);
    }

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "key_user", referencedColumnName = "key_user", nullable = false, insertable = false, updatable = false)
    public User getUserByKeyUser() {
        return userByKeyUser;
    }

    public void setUserByKeyUser(User userByKeyUser) {
        this.userByKeyUser = userByKeyUser;
    }

    @ManyToOne
    @JoinColumn(name = "id_phone_type", referencedColumnName = "id_phone_type", nullable = false, insertable = false, updatable = false)
    public CatPhoneType getCatPhoneTypeByIdPhoneType() {
        return catPhoneTypeByIdPhoneType;
    }

    public void setCatPhoneTypeByIdPhoneType(CatPhoneType catPhoneTypeByIdPhoneType) {
        this.catPhoneTypeByIdPhoneType = catPhoneTypeByIdPhoneType;
    }

    @Basic
    @Column(name = "key_user")
    public Integer getKeyUser() {
        return keyUser;
    }

    public void setKeyUser(Integer keyUser) {
        this.keyUser = keyUser;
    }

    @Basic
    @Column(name = "id_phone_type")
    public Integer getIdPhoneType() {
        return idPhoneType;
    }

    public void setIdPhoneType(Integer idPhoneType) {
        this.idPhoneType = idPhoneType;
    }
}
