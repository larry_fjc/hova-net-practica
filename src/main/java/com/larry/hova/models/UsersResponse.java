package com.larry.hova.models;

import java.util.List;

public class UsersResponse {

    private List<User> items;
    private int results;

    public List<User> getItems() {
        return items;
    }

    public void setItems(List<User> items) {
        this.items = items;
    }

    public int getResults() {
        return results;
    }

    public void setResults(int results) {
        this.results = results;
    }
}
