package com.larry.hova.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cat_gender", schema = "hova_net", catalog = "")
public class CatGender {
    private Integer idGender;
    private String description;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_gender")
    public Integer getIdGender() {
        return idGender;
    }

    public void setIdGender(Integer idGender) {
        this.idGender = idGender;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CatGender catGender = (CatGender) o;
        return Objects.equals(idGender, catGender.idGender) &&
                Objects.equals(description, catGender.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idGender, description);
    }
}
