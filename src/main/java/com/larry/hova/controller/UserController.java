package com.larry.hova.controller;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.larry.hova.models.CatPhoneType;
import com.larry.hova.models.User;
import com.larry.hova.models.UsersResponse;
import com.larry.hova.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/v1/users")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping(path = "/list")
    public UsersResponse getUsers(@RequestBody ObjectNode body) {
       return userService.getUsers(body);
    }

    @PostMapping(path = "/saveOrUpdate")
    public ResponseEntity<ObjectNode> saveOrUpdate(@RequestBody User user) {
        ObjectNode objectNode = userService.saveOrUpdate(user);
        if (objectNode.get("pin").asBoolean()) {
            objectNode.remove("pin");
            return new ResponseEntity<ObjectNode> (objectNode, HttpStatus.OK);
        } else {
            objectNode.remove("pin");
            return new ResponseEntity<ObjectNode> (objectNode, HttpStatus.valueOf(400));
        }
    }
}
