package com.larry.hova.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cat_workshift", schema = "hova_net", catalog = "")
public class CatWorkshift {
    private Integer idWorkshift;
    private String description;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_workshift")
    public Integer getIdWorkshift() {
        return idWorkshift;
    }

    public void setIdWorkshift(Integer idWorkshift) {
        this.idWorkshift = idWorkshift;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CatWorkshift that = (CatWorkshift) o;
        return Objects.equals(idWorkshift, that.idWorkshift) &&
                Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idWorkshift, description);
    }
}
