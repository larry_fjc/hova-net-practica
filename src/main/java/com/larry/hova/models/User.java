package com.larry.hova.models;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
public class User {
    private Integer keyUser;
    private String name;
    private String lastname;
    private String surname;
    private String rfc;
    private Integer gender;
    private Integer workshift;
    private Long pin;
    private Date birthdate;
    private CatWorkshift catWorkshiftByWorkshift;
    private CatProfile catProfileByProfile;
    private Collection<UserPhone> userPhonesByKeyUser;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "key_user")
    public Integer getKeyUser() {
        return keyUser;
    }

    public void setKeyUser(Integer keyUser) {
        this.keyUser = keyUser;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "lastname")
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Basic
    @Column(name = "surname")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Basic
    @Column(name = "rfc")
    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    @Basic
    @Column(name = "gender")
    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    @Basic
    @Column(name = "pin")
    public Long getPin() {
        return pin;
    }

    public void setPin(Long pin) {
        this.pin = pin;
    }

    @Basic
    @Column(name = "birthdate")
    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(keyUser, user.keyUser) &&
                Objects.equals(name, user.name) &&
                Objects.equals(lastname, user.lastname) &&
                Objects.equals(surname, user.surname) &&
                Objects.equals(rfc, user.rfc) &&
                Objects.equals(gender, user.gender) &&
                Objects.equals(pin, user.pin) &&
                Objects.equals(birthdate, user.birthdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(keyUser, name, lastname, surname, rfc, gender, pin, birthdate);
    }

    @ManyToOne
    @JoinColumn(name = "workshift", referencedColumnName = "id_workshift", nullable = false, insertable = false, updatable = false)
    public CatWorkshift getCatWorkshiftByWorkshift() {
        return catWorkshiftByWorkshift;
    }

    public void setCatWorkshiftByWorkshift(CatWorkshift catWorkshiftByWorkshift) {
        this.catWorkshiftByWorkshift = catWorkshiftByWorkshift;
    }

    @ManyToOne
    @JoinColumn(name = "profile", referencedColumnName = "id_profile")
    public CatProfile getCatProfileByProfile() {
        return catProfileByProfile;
    }

    public void setCatProfileByProfile(CatProfile catProfileByProfile) {
        this.catProfileByProfile = catProfileByProfile;
    }

    @Basic
    @Column(name = "workshift")
    public Integer getWorkshift() {
        return workshift;
    }

    public void setWorkshift(Integer workshift) {
        this.workshift = workshift;
    }

    @OneToMany(mappedBy = "userByKeyUser")
    public Collection<UserPhone> getUserPhonesByKeyUser() {
        return userPhonesByKeyUser;
    }

    public void setUserPhonesByKeyUser(Collection<UserPhone> userPhonesByKeyUser) {
        this.userPhonesByKeyUser = userPhonesByKeyUser;
    }
}
